package com.example.android.guesstheword.screens.game

import android.os.CountDownTimer
import android.text.format.DateUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel


private val CORRECT_BUZZ_PATTERN = longArrayOf(100, 100, 100, 100, 100, 100)
private val PANIC_BUZZ_PATTERN = longArrayOf(0, 200)
private val GAME_OVER_BUZZ_PATTERN = longArrayOf(0, 2000)
private val NO_BUZZ_PATTERN = longArrayOf(0)

class GameViewModel:ViewModel() {

    companion object {
        // These represent different important times
        // This is when the game is over
        const val DONE = 0L
        // This is the number of milliseconds in a second
        const val ONE_SECOND = 1000L
        // This is the total time of the game
        const val COUNTDOWN_TIME = 10000L
    }

    enum class BuzzType(val pattern: LongArray) {
        CORRECT(CORRECT_BUZZ_PATTERN),
        GAME_OVER(GAME_OVER_BUZZ_PATTERN),
        COUNTDOWN_PANIC(PANIC_BUZZ_PATTERN),
        NO_BUZZ(NO_BUZZ_PATTERN)
    }

    // The current word
    private val _word = MutableLiveData<String>()
    val word : LiveData<String>
        get() = _word
    // The current score
    private val _score = MutableLiveData<Int>()
    val score : LiveData<Int>
        get() = _score
    // The game finished Boolean
    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish : LiveData<Boolean>
        get() = _eventGameFinish
    // Timer
    private val _timer = MutableLiveData<Long>()
    val timer : LiveData<Long>
        get() = _timer
    // Timer(String)
    val timerString = Transformations.map(timer,
            {time -> DateUtils.formatElapsedTime(time)})
    // The Vibrator Pattern
    val buzzPattern = MutableLiveData<LongArray>()

    // The list of words - the front of the list is the next word to guess
    private lateinit var wordList: MutableList<String>
    private val timerCount: CountDownTimer

    /**
     * Resets the list of words and randomizes the order
     */
    private fun resetList() {
        wordList = mutableListOf(
                "queen",
                "hospital",
                "basketball",
                "cat",
                "change",
                "snail",
                "soup",
                "calendar",
                "sad",
                "desk",
                "guitar",
                "home",
                "railway",
                "zebra",
                "jelly",
                "car",
                "crow",
                "trade",
                "bag",
                "roll",
                "bubble"
        )
        wordList.shuffle()
    }

    /**
     * Moves to the next word in the list
     */
    private fun nextWord() {
        //Select and remove a word from the list
        if (wordList.isEmpty()) {
            resetList()
        } else {
            _word.value = wordList.removeAt(0)
        }
    }

    /** Methods for buttons presses **/

    fun onSkip() {
        _score.value = (_score.value)?.minus(1)
        nextWord()
    }

    fun onCorrect() {
        _score.value = (_score.value)?.plus(1)
        nextWord()
        buzzPattern.value = BuzzType.CORRECT.pattern
    }


    init {
        resetList()
        nextWord()
        Log.i("GameViewModel","GameViewModel Created")
        _word.value = ""
        _score.value = 0
        _eventGameFinish.value = false
        buzzPattern.value = BuzzType.NO_BUZZ.pattern

        timerCount = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {

            override fun onTick(millisUntilFinished: Long) {
               _timer.value = millisUntilFinished/1000
                if ((millisUntilFinished/1000) <= 5.0){
                    buzzPattern.value = BuzzType.COUNTDOWN_PANIC.pattern
                }
            }

            override fun onFinish() {
                _eventGameFinish.value = true
                buzzPattern.value = BuzzType.GAME_OVER.pattern
            }
        }

        timerCount.start()
    }
    override fun onCleared() {
        super.onCleared()
        timerCount.cancel()
        Log.i("GameViewModel","GameViewModel Destroyed")
    }

    fun eventGameFinished(){
        _eventGameFinish.value = false
    }
}